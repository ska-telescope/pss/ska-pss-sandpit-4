/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FLDO_DEVICEFUNCTION_CU_H
#define SKA_CHEETAH_MODULES_FLDO_DEVICEFUNCTION_CU_H

#include "cheetah/modules/fldo/cuda/detail/DeviceMem.h"
#include "cheetah/modules/fldo/cuda/src/LoadConstantData.cu"
#include "cheetah/modules/fldo/cuda/src/kernels/Kernels.cu"
#include "cheetah/modules/fldo/cuda/src/kernels/FoldInputDataKernel.cu"
#include "cheetah/modules/fldo/cuda/src/kernels/RebinInputDataKernel.cu"
#include "cheetah/modules/fldo/cuda/src/SummaryStatistics.cu"
#include "cheetah/modules/fldo/cuda/src/RebinInputData.cu"
#include "cheetah/modules/fldo/cuda/src/CornerTurner.cu"
#include "cheetah/modules/fldo/cuda/src/FoldInputData.cu"
#include "cheetah/modules/fldo/cuda/src/CandidatesProfiles.cu"
#include "cheetah/modules/fldo/cuda/src/Optimization.cu"

#endif // SKA_CHEETAH_MODULES_FLDO_DEVICEFUNCTION_CU_H
