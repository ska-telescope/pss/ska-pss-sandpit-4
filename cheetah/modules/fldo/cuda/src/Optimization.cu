
/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/fldo/cuda/detail/FldoUtils.h"
#include "cheetah/modules/fldo/cuda/detail/Optimization.h"
#include <vector>


namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cuda {
namespace util {

/*
 *
 * void phase_shift_calc(int index, float* phase_shift, double *nu, float delta_dm, float delta_nu,
 *                     float delta_nudot, double *time_sum, double
 *                     *freq_sum)
 */
/**
 *
 * @brief For each candidate and for each perturbation of the pulsar parameters,
 * the function calculates the shift in phase and store it in an array.
 * The array  is then copied into the GPU global memory and passed as
 * argument to the kernel that perturbs the profiles.
 *
 * @param index         the global index to address the list of three * parameters  (dm, nu, nudot)
 * @param ncand         the number of the pulsar candidate
 * @param phase_shift   the arry to store the shifts
 * @param nu            the array with freq. values of the candidates
 * @param delta_dm      the perturbation on DM
 * @param delta_nu      the perturbation on freq.
 * @param delta_nudot   the perturbation on nudot
 * @param time_sum      the array with sub-integration central time
 * @param freq_sum      the array with sub-band central freq.
 */
void phase_shift_calc(int ncand, int nsubbands, int nsubints, std::vector<float> &phase_shift, double period,
                      float delta_dm, float delta_nu, float delta_nudot,
                      const double* time_sum, const double* freq_sum, int index)
{
    double phshift = 0.;

    for (int iband = 0; iband < nsubbands; iband ++) {
        double time_shift = delta_dm * (1./(freq_sum[0] * freq_sum[0]) - 1./(freq_sum[iband] * freq_sum[iband])) * 1./period;
        for (int isub = 0; isub <nsubints; isub ++) {
            phshift = time_shift - (delta_nu * time_sum[isub]  + time_sum[isub] * time_sum[isub] * delta_nudot * 0.5);
            if (phshift < 0) {
                phshift += (-1.*floor(phshift));
            }
            phase_shift[iband + isub * nsubbands + ncand * nsubbands * nsubints] = (float)phshift;

        }
    }
}
/*
 *
 * void perturb_candidates(data::Scl const& ori_data, data::Scl &opt_data,
 *       float *d_folded, float* d_perturbed, float *d_outfprof,
 *       float *d_opt_profile, std::vector<util::GpuStream>& exec_stream,
 *       const std::vector<double> const& time_sum, std::vector <double> const& freq_sum,
 *       std::vector <float> &trial_param, int nchannel,
 *       int nsubints, int nsubbands, int default_max_phases, double tobs, int p_trial, int p_rf,
 *       int pdot_trial, int pdot_rf, int dm_trial, int dm_rf)
 *
 * @brief Build the perturbed profile of all candidates applying small perturbations to the
 * pulsar period, dispersion and pdot. For each candidate produce the
 * reduced profile.
 *
 * @param ori_data      the list with pulsar candidates original values
 * @param opt_data      the list with pulsar candidates optimized values
 * @d_folded            the array in GPU global mem with the folded data for each cabdidate
 * @d_perturbed         the array in GPU global with the perturbed
 *                      profiles (one for each subband, sub integration and candidate)
 * @d_outfprof          the array in GPU global mem with the perturbed profiles in freq
 * @d_optfprof          the array in GPU global mem with the final reduced profile used
 *                      during the final optimization phase.
 * @exec_stream         A vector with the GPU streams where the kernels are
 *                      loaded and executedd for the different  candidates.
 * @param time_sum      the array with sub-integration central time
 * @param freq_sum      the array with sub-band central freq.
 * @trial_param         vector with all the 3-tuples of pulsar parameters
 * @param p_trial       number of for period optimization
 * @param p_rf          reduce factor of the period step wiidth
 * @param pdot_trial    number of steps in pdot optimization
 * @param pdot_rf       reduce factor of the pdot step width
 * @param dm_trial      number of steps in dm optimization
 * @param dm_rf         reduce factor of the dm step width
 *
 * @return On failure throws a runtime_error exception.
 */
void perturb_candidates(data::Scl const& ori_data, data::Scl &opt_data,
        float *d_folded, float* d_perturbed, float *d_outfprof,
        float *d_opt_profile, std::vector<util::GpuStream>& exec_stream,
        std::vector<double> const& time_sum, std::vector <double> const& freq_sum,
        std::vector <float> &trial_param, int nchannel,
        int nsubints, int nsubbands, int default_max_phases, double tobs, int p_trial, int p_rf,
        int pdot_trial, int pdot_rf, int dm_trial, int dm_rf)

{
    int nperiod = 0, npdot = 0, ndm = 0;
    float delta_dm = 0.;                // dm perturbation value
    float delta_nu = 0.;                // nu perturbation value
    float delta_nudot = 0.;             // nudot perturbation value
    float period_range= 0.000202e-3;    // period range and step for optimization phase
    float period_step = 0.00002e-3;     // period step
    float pdot_range  = 0.0202e-6;      // pdot range and step for optimization phase
    float pdot_step   = 0.02e-7;        // pdot step
    float dm_range    = 1.0;            // dm range and step for optimization phase
    float dm_step     = 0.10;           // dm step
    float trial_period = 0., trial_pdot = 0., trial_dm = 0.;
    dim3 threadsPerBlock;                       //number of threads for each block
    dim3 blocksPerGrid;


    // calculate number of trials for period, pdot and dm
    nperiod = p_trial * 2 - 1;
    npdot   = pdot_trial * 2 - 1;
    ndm     = dm_trial * 2 - 1;

    //allocate a vector to store the phase shift for each candidate and trial
    size_t const ncandidates = ori_data.size();
    int trials_num = nperiod * npdot * ndm;  //total numer of trials for each candidate
    size_t ph_size = ncandidates * nsubbands * nsubints;
    std::vector<float>phase_shift(ph_size);
    //Note: the size of the vector is = (trials_num * 3) because for each
    //trial we register the trial period, the trial dm and trial pdot.
    //std::vector<float>trial_param(3 * trials_num * ncandidates);
    trial_param.resize(3 * trials_num * ncandidates);
    int index = 0;

    // pointer to device memory to store phase shifts in optimization phase
    // d_phase_shft is used only by this routine, so we allocate it here.
    // It is automatically released at routine exiting.
    panda::nvidia::CudaDevicePointer<float> d_phase_shift(ph_size);
    //Oss: device memory allocated with cudaMalloc is not cleared
    CUDA_ERROR_CHECK(cudaMemset((void*)d_phase_shift(), 0, ph_size* sizeof(float)));

    try {
        for (int ip = 0; ip < nperiod; ip++) {
            PANDA_LOG_DEBUG << " iteration " << ip << "\\" << nperiod;
            for (int ipdot = 0; ipdot < npdot; ipdot++) {
                for (int idm = 0; idm < ndm; idm++) {
                    for (size_t ncand = 0; ncand < ncandidates ; ncand ++) {
                        double const folded_cand_period =
                            boost::units::quantity_cast<double>
                                    (static_cast<boost::units::quantity<boost::units::si::time>>((ori_data[ncand].period())));
                        double opt_cand_period =
                            boost::units::quantity_cast<double>
                                    (static_cast<boost::units::quantity<boost::units::si::time>>((opt_data[ncand].period())));
                        period_step = folded_cand_period/tobs/5000.;
                        if (p_rf > 1) {
                            period_step /= p_rf;
                        }
                        // to mantain the same number of optimization points, we update the period_range
                        // (not used hereafter)
                        period_range = p_trial * period_step;
                        double const folded_cand_pdot = (ori_data[ncand].pdot()).value();
                        double opt_cand_pdot = (opt_data[ncand].pdot()).value();

                        pdot_step = folded_cand_period/tobs/tobs/1000 * 2;
                        if (pdot_rf > 1) {
                            pdot_step /= pdot_rf;
                        }
                        // to mantain the same number of optimization points, we update the pdot_range
                        // (not used hereafter)
                        pdot_range = pdot_trial * pdot_step;

                        float const folded_cand_dm = (ori_data[ncand].dm()).value();
                        float opt_cand_dm = (opt_data[ncand].dm()).value();
                        dm_step = folded_cand_period * 90;
                        // we report limits from Story AT4-18
                        dm_step = 80. * folded_cand_period * (folded_cand_period/1000.) ;
                        dm_step = ((dm_step > .5) ? .5: dm_step);
                        if (dm_step > .5)
                            dm_step = 0.5;
                        dm_step = ((dm_step < .001) ? .001: dm_step);
                        if (dm_step < .001)
                            dm_step = .001;
                        // to mantain the same number of optimization points, we update the dm_range
                        // (not used hereafter)
                        dm_range = dm_trial * dm_step;
                        if (dm_rf > 1) {
                                dm_step /= dm_rf;
                        }
                        // w
                        PANDA_LOG_DEBUG << " ranges " << period_range << pdot_range << dm_range;

                        // the index into the p/dm and p/pdot planes. note that we use result->nperiod not n_p
                        // because of the factor of 2 above.
                        trial_period = (ip - p_trial + 1) * period_step + opt_cand_period;
                        trial_pdot = (ipdot - pdot_trial + 1) * pdot_step + opt_cand_pdot;
                        trial_dm = (idm - dm_trial + 1) * dm_step + opt_cand_dm;
                        delta_dm = (folded_cand_dm  - trial_dm) * fldo::k_dm;
                        delta_nu = (1.0/trial_period) - 1./folded_cand_period;
                        delta_nudot = -trial_pdot /(trial_period * trial_period) +
                                      folded_cand_pdot/(folded_cand_period*folded_cand_period);
                        phase_shift_calc(ncand, nsubbands, nsubints, phase_shift, opt_cand_period, delta_dm, delta_nu,
                                         delta_nudot, time_sum.data(), freq_sum.data() ,index);
                        int shift = ncand * nsubbands * nsubints;
                        int nstream = ncand % exec_stream.size();
                        CUDA_ERROR_CHECK(cudaMemcpyAsync(d_phase_shift() + shift, &phase_shift[shift],
                                         nsubbands * nsubints* sizeof(float), cudaMemcpyHostToDevice,
                                         exec_stream[nstream].stream()));
                        //CUDA_ERROR_CHECK(cudaGetLastError());
                        //configure the perturbed kernel parameters
                        threadsPerBlock.x = 32;     // threadIdx.x -> phase bin
                        threadsPerBlock.y = 4;      // threadIdx.y -> subband
                        //small speed up but results screwed up
                        blocksPerGrid.x = nsubbands/threadsPerBlock.y;      // one block per subband
                        blocksPerGrid.y = nsubints;                         // one block per subints
                        size_t shared_memsize  = default_max_phases * sizeof(float) *threadsPerBlock.y;
                        // rotate the profiles to account for the small change in p/pdot/dm.
                        perturbate_kernel<<< blocksPerGrid, threadsPerBlock, shared_memsize,
                                             exec_stream[nstream].stream() >>> (d_folded,
                                                                              d_perturbed,
                                                                         d_phase_shift(),
                                                                                   ncand,
                                                                      default_max_phases,
                                                                                  index);
                        CUDA_ERROR_CHECK(cudaGetLastError());

                        //reduce kernel to build the profile in freq.
                        threadsPerBlock.x = default_max_phases;
                        threadsPerBlock.y = 1;
                        blocksPerGrid.x = nsubints;
                        blocksPerGrid.y = 1;
                        profile_kernel<<< blocksPerGrid, threadsPerBlock, 0,
                             exec_stream[nstream].stream() >>> (d_perturbed,
                                                                 d_outfprof,
                                                                      ncand,
                                                                 nsubbands);
                        CUDA_ERROR_CHECK(cudaGetLastError());
                        threadsPerBlock.x = default_max_phases;
                        threadsPerBlock.y = 1;
                        blocksPerGrid.x = 1; // not better if fold.nsubints;
                        blocksPerGrid.y = 1;
                        //reduce kernel to build the final profile
                        best_profile_kernel<<< blocksPerGrid, threadsPerBlock, 0,
                                               exec_stream[nstream].stream()>>>(d_outfprof,
                                                                           d_opt_profile,
                                                                                   ncand,
                                                                                nsubints,
                                                                                   index,
                                                                             trials_num);
                        CUDA_ERROR_CHECK(cudaGetLastError());
                        //store the trial values into the allocated array
                        //
                        // arrangment of data in trial_param array
                        // --------------------------------------------------------------------------------
                        // |period0| pdot0| dm0 | period0 | pdot0 | dm1 | .........|periodN | pdotM | dmR |
                        // -------------------------------------------------------------------------------
                        //      index 0         |    index 1            | .........|     index N*M*R      |
                        // <-------------------------------cand 0----------------------------------------->
                        // This is repeated for each candidate, so the final array contains al the trial parameters for all
                        // the candidates
                        trial_param[index * 3 + ncand * trials_num * 3] = trial_period;
                        trial_param[index * 3 + ncand * trials_num * 3 + 1] = trial_pdot;
                        trial_param[index * 3 + ncand * trials_num * 3 + 2] = trial_dm;
                    }   //end loop on candidates
                    CUDA_ERROR_CHECK(cudaDeviceSynchronize());
                    index ++;
                }       // end loop on dm perturbations
            }           // end loop on pdot perturbations
        }               // end loop on period perturbations
    }
    catch (std::runtime_error &e)
    {
        PANDA_LOG_ERROR << "Caught an exception of an unexpected type in perturb_candidates(): "
                        << e.what();
        throw e;
    }
    catch (...)
    {
        PANDA_LOG_ERROR << "Catch unknown exception in perturb_candidates()";
        throw panda::Error("Catch unknown exception in perturb_candidates()");
    }
}


/*
 *
 * void compute_max_sn(size_t const ncandidates, int trials_num, int default_max_phases, float sigma, float
 *       *d_opt_profile,  std::vector<float> &sn_max, std::vector<float> &pulse_width, std::vector<int> &index_max,
 *        std::vector<util::GpuStream>& exec_stream)
 * @brief Evaluates the best S/N value for each pulsar candidate.
 *
 * @param ncandidates   the total number of pulsar candidates
 * @trials_num          the number of trials done by the optimization procedure
 * @default_max_phases  the max number of phase bins
 * @sigma               the standard deviation value of the processed data
 * @d_opt_profile       the array (in GPU memory) with the optimized
 *                      schrunched profiles (in freq and time) of all the candidates
 * @sn_max              A vectory with the best S/N values for each candidate
 * @pulse_width         A vectory with the pulse width (corresponding to the * best S/N value) for each candidate
 * @index_max           A vectory with the index of the 3-tupla of trial params (period, pdot and dm) that for
 *                      each candidate correspnds the best S/N value
 * @exec_stream         A vector with the GPU streams where the kernels are
 *                      loaded and executedd for the different  candidates.
 * @return On failure throws a runtime_error exception.
*/
void compute_max_sn(size_t const ncandidates, int trials_num, int default_max_phases, float sigma, float
        *d_opt_profile,  std::vector<float> &sn_max, std::vector<float> &pulse_width, std::vector<int> &index_max,
        std::vector<util::GpuStream>& exec_stream)
{
    try
    {
        size_t opt_value_size =  2 * ncandidates *trials_num;
        // pointer to the optimized values of all candidates
        panda::nvidia::CudaDevicePointer<float>d_opt_value(opt_value_size);
        CUDA_ERROR_CHECK(cudaMemset(d_opt_value(), 0, opt_value_size * sizeof(float)));
        int nblock_y = 1;
        int nthread_x = 32;
        if (trials_num > nthread_x) {
            nblock_y = trials_num/nthread_x + 1;
        }
        dim3 threadsPerBlock;       //number of threads for each block
        dim3 blocksPerGrid;
        threadsPerBlock.x = nthread_x;
        threadsPerBlock.y = 1;
        blocksPerGrid.x = nblock_y;
        //trials_num * 2 -> for each trial, we store the S/N of the profile and width value
        int shared_memsize = nthread_x * 2 * sizeof(float);
        // to get the correct sigma value, we need to scale the calculated
        // sigma with the first prebin value. The first prebin value defines the input
        // data array dimension on which the statistics is evaluated
        for (size_t ncand = 0; ncand < ncandidates; ncand ++) {
            int nstream = ncand % exec_stream.size();
            compute_sn<<<blocksPerGrid, threadsPerBlock, shared_memsize, exec_stream[nstream].stream()
                                                                                  >>> (d_opt_profile,
                                                                                         d_opt_value(),
                                                                                    default_max_phases,
                                                                                                 ncand,
                                                                                            trials_num,
                                                                                                sigma);
            CUDA_ERROR_CHECK(cudaGetLastError());
            // Warning!!! Last value should read sigma*sqrt((float)  (nbins[ncand] * first_prebin))
            // but here we do not have nbins[], so we make computation inside compute_sn
        }

        //
        // arrangment of data stored in d_opt_value (GPU mem) and h_opt_value (RAM)
        //
        // -------------------------------------------------------------------|
        // sn_0 |sn_1 | sn_2| .......|sn_Ntrial| w_0|w_1| w_2|......|w_Ntrial |
        // ____________________________________|______________________________|
        //  <---- N trial elements          -->|<------ N trial elements    ->|
        //<------------------------------ cand 0 ----------------------------->
        //

        // d_opt_value and h_opt_value have 2 * N_trial elements for each pulsar candidate
        // d_opt_value is passed to the thrust routine to find the S/N max value and its corresponding
        // array index.The positionof the max S/N corresponds to the index of the 3-tuple stored into trial_param array.
        //
        for (int ncand = 0; ncand < ncandidates; ncand ++) {
            // start is the index of the data relative to the ncand candidate
            // find max returns the position of the max S/N value relative to start
            int start = ncand * trials_num  * 2 ;
            util::find_max(d_opt_value(), start, trials_num, sn_max[ncand], pulse_width[ncand], index_max[ncand]);
            PANDA_LOG_DEBUG << "Candidate: " << ncand
                            << " sn_max: " << sn_max[ncand]
                            << " index: " << index_max[ncand];
        }
    }
    catch (std::runtime_error &e)
    {
        PANDA_LOG_ERROR << "Caught an exception of an unexpected type in compute_max_sn(): "
                        << e.what();
        throw e;
    }
    catch (...)
    {
        PANDA_LOG_ERROR << "Catch unknown exception in compute_max_sn()";
        throw panda::Error("Catch unknown exception in compute_max_sn()");
    }

}
/*
 * int first_valid_binidx(std::vector<util::CandidateRebin> &rebin)
 *
 * @brief utility function to get the first valid rebinning value, i.e. the
 * first entry in the rebin vector with a number of candidate > 0
 *
 * @rebin       the input rebin vector
 * @return      the element index
 */
int first_valid_binidx(std::vector<util::CandidateRebin> &rebin)
{
    int index = 0;
    for (std::vector<util::CandidateRebin>::iterator it = rebin.begin(); it != rebin.end(); ++it) {
        if ((*it).first > -1) {
            //get the index of the iterator
            //return std::distance(it, rebin.begin());
            return index;
        }
        index++;
    }
    return -1;
}


} // utils
} // namespace cuda
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska
