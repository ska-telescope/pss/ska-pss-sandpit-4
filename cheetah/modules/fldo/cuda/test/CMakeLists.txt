include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_fldo_cuda_src
    src/CudaTest.cu
    src/gtest_fldo_cuda.cpp
)

if(ENABLE_CUDA)
    if(DEEP_TESTING)
        add_definitions(-DDEEP_TESTING)
    endif()
    cuda_add_executable(gtest_fldo_cuda ${gtest_fldo_cuda_src})
    target_link_libraries(gtest_fldo_cuda ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
    add_test(gtest_fldo_cuda gtest_fldo_cuda --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
    # Create a standard candidate file
    configure_file(data/get_testvectors.sh get_testvectors.sh COPYONLY)
else()
    if(DEEP_TESTING)
        message( "fldo: DEEP_TESTING requires CUDA")
    endif()
endif()
