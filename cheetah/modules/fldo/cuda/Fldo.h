/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FLDO_CUDA_FLDO_H
#define SKA_CHEETAH_MODULES_FLDO_CUDA_FLDO_H

#include "cheetah/Configuration.h"
#include "cheetah/modules/fldo/Config.h"
#include "cheetah/modules/fldo/cuda/Config.h"
#include "cheetah/modules/fldo/Types.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cuda {

/**
 * @brief CPU-only class that returns an empty Ocld
 *
 * @details If CUDA is enabled, but the input datatype is not supported by the CUDA algo,
 *          this method will be compiled. The constructor will tell you this, and the
 *          operator will return an empty Ocld.
 *
 *          If Cuda is not enabled, the constructor will tell you that no CUDA algos have
 *          been compiled, and return an empty Ocld.
 */
template<typename FldoTraits, typename Enable = void>
class Fldo
{
    public:
        typedef cheetah::Cpu Architecture;
        typedef panda::PoolResource<Architecture> ResourceType;
        typedef cuda::Config Config;
        typedef FldoTraits Traits;

    private:
        typedef typename FldoTraits::TimeFrequencyType TimeFrequencyType;

    public:
        Fldo(fldo::Config const&) {
#ifdef SKA_CHEETAH_ENABLE_CUDA
            throw panda::Error("Fldo cuda origami algo does not exist for this data type");
#else  // SKA_CHEETAH_ENABLE_CUDA
            throw panda::Error("Fldo cuda (origami) algo has not been compiled. Recompile with cmake -DENABLE_CUDA=true option");
#endif // SKA_CHEETAH_ENABLE_CUDA
        };

        // Null operator; return a shared pointer to an empty Ocld
        std::shared_ptr<data::Ocld<typename FldoTraits::value_type>> operator()( ResourceType&
                                                                               , std::vector<std::shared_ptr<TimeFrequencyType>> const&
                                                                               , data::Scl const&)
        {
            std::shared_ptr<data::Ocld<typename FldoTraits::value_type>> output = std::make_shared<data::Ocld<typename FldoTraits::value_type>>();
            return output;
        }
};

} // namespace cuda
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska

#ifdef SKA_CHEETAH_ENABLE_CUDA
#include "cheetah/modules/fldo/cuda/detail/Fldo.cuh"

namespace ska {
namespace cheetah {
namespace modules {
namespace fldo {
namespace cuda {

/**
 * @brief CUDA implementations enabled if input datatype is 8 bit
 *
 * @details If CUDA is enabled and input datatype is 8 bit (specifically
 *          uint8_t), call the Fldo algo to fold the input data with the
 *          parameters in the candidate list.
 *
 */
template<typename FldoTraits>
class Fldo< FldoTraits
          , typename std::enable_if<
                         std::is_same<typename FldoTraits::value_type, uint8_t>::value
                     >::type
          >
    : public detail::Fldo<FldoTraits>
{
        typedef detail::Fldo<FldoTraits> BaseT;

    public:
        typedef cheetah::Cuda Architecture;
        typedef typename BaseT::ArchitectureCapability ArchitectureCapability;

        typedef panda::PoolResource<Architecture> ResourceType;
        typedef typename BaseT::Config Config;
        typedef FldoTraits Traits;

    public:
        using detail::Fldo<FldoTraits>::Fldo;

        /**
         * @brief Call the CUDA kernel to perform folding
         */
        inline
        std::shared_ptr<data::Ocld<typename FldoTraits::value_type>> operator()( ResourceType& res
                                                                               , std::vector<std::shared_ptr<data::TimeFrequency<Cpu, uint8_t>>> const& data
                                                                               , data::Scl const& scl)
        {
            return BaseT::operator()(res, data, scl);
        }
};

} // namespace cuda
} // namespace fldo
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif //SKA_CHEETAH_ENABLE_CUDA

#endif // SKA_CHEETAH_MODULES_FLDO_CUDA_FLDO_H
