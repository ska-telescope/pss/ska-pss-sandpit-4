/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_HRMS_TDT_REFERENCE_H
#define SKA_CHEETAH_MODULES_HRMS_TDT_REFERENCE_H

#include "cheetah/modules/tdrt/FrequencySeries.h"
#include "cheetah/modules/tdrt/TimeSeries.h"
#include <vector>

namespace ska {
namespace cheetah {
namespace modules {
namespace hrms {
namespace test {

/**
 * @brief Harmonic Summing Reference Model
 *
 * @details
 */

class HrmsTDTReference
{
    public:

    typedef enum {
        one =    0,
        two =    1,
        four =   2,
        eight=   3,
        sixteen= 4
    } Harmonics;

    HrmsTDTReference(Harmonics harmonics);

    void process(modules::tdrt::FrequencySeries<float>& h_input,
                 std::vector<std::shared_ptr<modules::tdrt::FrequencySeries<float>>>& h_output);

    void harmonics_train(size_t period, modules::tdrt::FrequencySeries<float>& h_harmonics_train_out);

    private:
        Harmonics _harmonics;
};

} // test
} // namespace hrms
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_HRMS_TDT_REFERENCE_H