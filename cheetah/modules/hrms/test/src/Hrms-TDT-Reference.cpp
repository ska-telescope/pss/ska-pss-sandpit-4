/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "Hrms-TDT-Reference.h"
// uncomment to disable assert()
// #define NDEBUG
#include <cassert>
#include <cstring>

namespace ska {
namespace cheetah {
namespace modules {
namespace hrms {
namespace test {

HrmsTDTReference::HrmsTDTReference(Harmonics harmonics)
    :_harmonics(harmonics)
{
}

void HrmsTDTReference::process(tdrt::FrequencySeries<float>& h_input,
                      std::vector<std::shared_ptr<tdrt::FrequencySeries<float>>>& h_output)
{

   // std::cout << "_harmonics " << _harmonics << std::endl;
   // std::cout << "h_output.size() " << h_output.size() << std::endl;

    unsigned n_harmonics= _harmonics+1;
    assert(h_output.size() >= n_harmonics);

    size_t length = h_input.get_length();
    float delta_f = h_input.get_delta_f();

    for (std::vector<std::shared_ptr<tdrt::FrequencySeries<float>>>::const_iterator it = h_output.begin();
         it!=h_output.end(); ++it)
    {
        assert ((*it)->get_length() == length);
        assert ((*it)->get_delta_f() == delta_f);
    }

    // Reference section doing summing like sigprog sumhrm.f: -----------------------------------
    // (omitting to just copy the first harmonic sum)

    const float Pmax = 9.9999;                                // largest pulsar period to find
    size_t nf1 = 2 * length / (h_input.get_delta_f() * Pmax); // var from sigprog sumhrm.f

    //std::cout << "Pmax " << Pmax << std::endl;
    //std::cout << "nf1 " << nf1 << std::endl;

    for (unsigned fold= 0; fold <= _harmonics; ++fold)
    {
        float fval = 1 << fold;                               // 1,2,4,8,16 ...
        size_t NFS = fval*nf1 - fval/2;                       // var from sigprog sumhrm.f
        float xdiv = 1.0 / fval;                              // var from sigprog sumhrm.f

        //std::cout << "summing fold " << fold << " with fval " << fval << std::endl;
        //std::cout << "NFS " << NFS << std::endl;
        //std::cout << "xdiv " << xdiv << std::endl;

        for (size_t n = NFS; n < length; ++n)
        {
            h_output[fold]->get_data()[n] = 0.0;                          // zero init output array
            long long last_idx = -1;

            for (unsigned m=0; m < fval; ++m)                      // summing over 1,2,4,8,16 ...
            {
                float  stretch_idx_float = n * m * xdiv + 0.5;
                long long stretch_idx_int   = (size_t) stretch_idx_float;
                //std::cout << "stretch index " << stretch_idx_float <<" "<< stretch_idx_int << std::endl;
                if ( stretch_idx_int > 1 && stretch_idx_int != last_idx)
                {
                    h_output[fold]->get_data()[n] += h_input.get_data()[stretch_idx_int];  // summing
                }
                last_idx= stretch_idx_int;
            }

        }
    }
}

void HrmsTDTReference::harmonics_train(size_t period, tdrt::FrequencySeries<float>& h_harmonics_train_out)
{
    assert(h_harmonics_train_out.get_length() > period * 10);  // accept at least 10 harmonics
    assert(period > 32);                                     // accept at least 32 bins between harmonics

    float* fsHarmonics = h_harmonics_train_out.get_data();

    for(size_t i = 0; i < h_harmonics_train_out.get_length(); ++i)
    {
        // only 16 harmonics, and DC is removed
        if(i % period == 0 && i <= 16 * period && i > 10)
            fsHarmonics[i]=1.0;
        else
            fsHarmonics[i]=0.0;
    }
}

} // test
} // namespace hrms
} // namespace modules
} // namespace cheetah
} // namespace ska