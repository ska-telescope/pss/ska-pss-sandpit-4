#include "cheetah/modules/hrms/cuda/Hrms.cuh"

namespace ska {
namespace cheetah {
namespace modules {
namespace hrms {
namespace cuda {

Hrms::Hrms(Config const& config, hrms::Config const& algo_config)
    : utils::AlgorithmBase<Config, hrms::Config>(config,algo_config)
{
}

Hrms::~Hrms()
{
}

} // namespace cuda
} // namespace hrms
} // namespace modules
} // namespace cheetah
} // namespace ska
