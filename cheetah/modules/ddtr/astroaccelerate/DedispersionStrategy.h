/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_DEDISPERSIONSTRATEGY_H
#define SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_DEDISPERSIONSTRATEGY_H

#include "cheetah/modules/ddtr/DedispersionTrialPlan.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/TimeFrequency.h"
#include <memory>
#include <algorithm>
#include <limits>

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace astroaccelerate {

//still work to do to understand these variables. But as of now we are using the
// default values from the Meertrap astroaccelerate Params.h file .(Link of MeerTrap repo https://gitlab.com/MeerTRAP/astro-accelerate)
// It is essential that the params should match the values in astoaccelerate library.
//TODO: Most of the values can be obtained from the GPU attributes and the observation details
template <typename NumericalRep>
class DedispersionOptimizationParameters{
    public:
        static constexpr unsigned UNROLLS = 8;
        static constexpr unsigned SNUMREG = 8;
        static constexpr unsigned SDIVINT = 14;
        static constexpr unsigned SDIVINDM = 50;
        static constexpr float SFDIVINDM = 50.0f;
        static constexpr unsigned MIN_DMS_PER_SPS_RUN = 64;
};

template<class Arch, typename NumericalT, typename OptimizationParameterT=DedispersionOptimizationParameters<NumericalT>>
class DedispersionStrategy
{
        friend class DedispersionStrategy<cheetah::Cuda, NumericalT, OptimizationParameterT>;

    public:
        typedef NumericalT NumericalRep;
        typedef uint16_t GpuNumericalRep;
        typedef boost::units::quantity<data::MegaHertz, double> FrequencyType;
        typedef data::DedispersionMeasureType<float> Dm;
        typedef boost::units::quantity<data::dm_constant::s_mhz::Unit, double> DmConstantType;
        typedef boost::units::quantity<boost::units::si::time, double> TimeType;

    public:
        DedispersionStrategy(const data::TimeFrequency<Cpu,NumericalRep>& chunk, const ddtr::DedispersionTrialPlan& plan, std::size_t gpu_memory);
        DedispersionStrategy(DedispersionStrategy const&) = delete;
        ~DedispersionStrategy();

        /**
         * @brief Memory used for the GPU operations
         */
        std::size_t gpu_memory() const;

        /**
         * @brief The number of dm ranges
         */
        unsigned int range() const;

        /**
         * @brief vector of input bin sizes
         */
        std::vector<int> const& in_bin() const;

        /**
         * @brief vector of output bin sizes
         */
        std::vector<int> const& out_bin() const;

        /**
         * @brief Value used to make sure that dms from dm_low to dm_high are used
         */
        unsigned int maxshift() const;

        /**
         * @brief An array containing the lowest bound of each dm range
         */
        std::vector<float> const& dm_low() const;

        /**
         * @brief An array containing the highest bound of each dm range
         */
        std::vector<float> const& dm_high() const;

        /**
         * @brief An array containing the step size of each dm range
         */
        std::vector<float> const& dm_step() const;

        /**
         * @brief An array containing a constant associated with each channel to perform dedispersion algorithm
         */
        std::vector<float> const& dmshifts() const;

        /**
         * @brief An array containing the number of dms for each range
         */
        std::vector<int> const& ndms() const ;

        /**
         * @brief The maximum number of dm
         */
        unsigned int max_ndms() const;

        /**
         * @brief The total number of dm
         */
        unsigned int total_ndms() const;

        /**
         * @brief The highest dm value
         */
        Dm max_dm() const;

        /**
         * @brief The number of time samples required to search for a dm in each dm range
         */
        std::vector<std::vector<int>> const& t_processed() const;

        /**
         * @brief The number of IF channels
         */
        unsigned int nifs() const;
        void nifs(unsigned int value);

        /**
         * @brief Time sample value
         */
        TimeType tsamp() const;

        /**
         * @brief Frequency of the first channel
         */
        FrequencyType fch1() const;

        /**
         * @brief Frequency channel width
         */
        FrequencyType foff() const;

        /**
         * @brief The number of time samples
         */
        unsigned int nsamp() const;

        /**
         * @brief The number of frequency channels
         */
        unsigned int nchans() const;

        /**
         * @brief The number of chunks the data are divided in
         */
        unsigned int num_tchunks() const;

        data::DimensionSize<data::Time> dedispersed_samples();

        void resize(size_t const number_of_samples, size_t const gpu_memory);

        void exec_kernel(std::size_t dm_range_index
                       , data::FrequencyTime<Cuda, uint16_t> const& d_input
                       , float* d_output
                       , int t_processed
                       , float tsamp
                       ) const;

    private:
        int lineshift(std::size_t step_index, float tsamp) const;

    private:
        /**
         * @brief Computes the dedispersion strategy
         *
         */
        void make_strategy(size_t gpu_memory);

    private:
        std::size_t _gpu_memory;
        unsigned int _range;
        std::vector<Dm> _user_dm_low;
        std::vector<Dm> _user_dm_high;
        std::vector<Dm> _user_dm_step;
        std::vector<int> _in_bin;
        std::vector<int> _out_bin;
        unsigned int _maxshift;
        std::vector<float> _dm_low;
        std::vector<float> _dm_high;
        std::vector<float> _dm_step;
        std::vector<float> _dmshifts;
        std::vector<int> _ndms;
        unsigned int _num_tchunks;
        unsigned int _max_ndms;
        unsigned int _total_ndms;
        Dm _max_dm;
        std::vector<std::vector<int>> _t_processed;
        unsigned int _nifs;
        TimeType _tsamp;
        unsigned int _nsamp;
        unsigned int _nchans;
        std::vector<float> _bin_frequencies;
        FrequencyType _fch1;
        FrequencyType _foff;
        size_t _SPS_mem_requirement;
        std::size_t _dedispersed_time_samples;
        DmConstantType _dm_constant;
};



}//astroacclerate
} // namespace ddtr
} // namespace modules
}//cheetah
}//ska
#include "cheetah/modules/ddtr/astroaccelerate/detail/DedispersionStrategy.cpp"

#endif // SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_DEDISPERSIONSTRATEGY_H
