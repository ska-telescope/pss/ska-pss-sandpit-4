/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2021 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/data/DedispersionMeasure.h"
#include "cheetah/modules/ddtr/test_utils/DdtrTester.h"
#include "cheetah/data/TimeFrequency.h"
#include <memory>
#include <vector>


namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace astroaccelerate {
namespace test {

template <typename NumericalT>
struct AstroAccelerateTraits
    : public ddtr::test::DdtrTesterTraits<ddtr::astroaccelerate::Ddtr, NumericalT>
{
    typedef ddtr::test::DdtrTesterTraits<ddtr::astroaccelerate::Ddtr, NumericalT> BaseT;
    void configure(ddtr::Config& config) override {
        BaseT::configure(config);
        config.astroaccelerate_algo_config().active(true);
    }

    bool expected_to_pass(data::TimeFrequency<Cpu, NumericalT> const& data) override {
        if(data.number_of_channels()>8192 || data.number_of_channels()==0) return false;

        return (ceil(log2(data.number_of_channels())) == floor(log2(data.number_of_channels())));
    }
};

} // namespace test
} // namespace astroaccelerate
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace test {

typedef ::testing::Types<ddtr::astroaccelerate::test::AstroAccelerateTraits<uint8_t>> AstroAccelerateTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, DdtrTester, AstroAccelerateTraitsTypes);

} // namespace test
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
