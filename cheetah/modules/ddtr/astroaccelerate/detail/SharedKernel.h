/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_SHARED_KERNEL_H
#define SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_SHARED_KERNEL_H

#include "cheetah/data/cuda/FrequencyTime.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace astroaccelerate {
template<class, typename, typename> class DedispersionStrategy;

/**
 * @brief
 * @details
 */

template<typename NumericalT, typename OptimisationParamsT>
class SharedKernel
{
    public:
        static void exec(int dm_range
                       , DedispersionStrategy<Cpu, NumericalT, OptimisationParamsT> const& strategy
                       , data::FrequencyTime<Cuda, uint16_t> const& d_input
                       , float* d_output
                       , int t_processed
                       , float tsamp);
};

template<typename OptimisationParamsT>
class SharedKernel<uint8_t, OptimisationParamsT>
{
    public:
        static void exec(int dm_range
                       , DedispersionStrategy<Cpu, uint8_t, OptimisationParamsT> const& strategy
                       , data::FrequencyTime<Cuda, uint16_t> const& d_input
                       , float* d_output
                       , int t_processed
                       , float tsamp);
};

} // namespace astroaccelerate
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_DDTR_ASTROACCELERATE_SHARED_KERNEL_H
