/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_MODULES_DDTR_GPU_BRUTEFORCE_DEDISPERSIONSTRATEGY_H
#define SKA_CHEETAH_MODULES_DDTR_GPU_BRUTEFORCE_DEDISPERSIONSTRATEGY_H

#include "cheetah/modules/ddtr/DedispersionTrialPlan.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/TimeFrequency.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace gpu_bruteforce {

template<class ArchT, typename NumericalT>
class DedispersionStrategy
{
        friend class DedispersionStrategy<cheetah::Cuda, NumericalT>;

    public:
        typedef NumericalT NumericalRep;
        typedef boost::units::quantity<data::MegaHertz, double> FrequencyType;
        typedef data::DedispersionMeasureType<float> Dm;
        typedef boost::units::quantity<data::dm_constant::s_mhz::Unit, double> DmConstantType;
        typedef boost::units::quantity<boost::units::si::time, double> TimeType;

    public:
        DedispersionStrategy(data::TimeFrequency<Cpu,NumericalRep> const& chunk
                            , ddtr::DedispersionTrialPlan const& plan
                            , std::size_t gpu_memory);
        DedispersionStrategy(DedispersionStrategy const&) = delete;
        ~DedispersionStrategy();

        /**
         * @brief The number of dm ranges
         */
        unsigned int range() const;

        /**
         * @brief vector of input bin sizes
         */
        std::vector<int> const& in_bin() const;

        /**
         * @brief Value used to make sure that dms from dm_low to dm_high are used
         */
        unsigned int maxshift() const;

        /**
         * @brief An array containing the lowest bound of each dm range
         */
        std::vector<Dm> const& dm_low() const;

        /**
         * @brief An array containing the highest bound of each dm range
         */
        std::vector<Dm> const& dm_high() const;

        /**
         * @brief An array containing the step size of each dm range
         */
        std::vector<Dm> const& dm_step() const;

        /**
         * @brief An array containing a constant associated with each channel to perform dedispersion algorithm
         */
        std::vector<float> const& dmshifts() const;

        /**
         * @brief An array containing the number of dms for each range
         */
        std::vector<int> const& ndms() const ;

        /**
         * @brief The total number of dm
         */
        unsigned int total_ndms() const;

        /**
         * @brief The highest dm value
         */
        Dm max_dm() const;

        /**
         * @brief Time sample value
         */
        TimeType tsamp() const;

        /**
         * @brief The number of time samples
         */
        unsigned int nsamp() const;

        /**
         * @brief The number of frequency channels
         */
        unsigned int nchans() const;

        /**
         * @brief returns dedispersed samples per DM in dmtrials
         */
        data::DimensionSize<data::Time> dedispersed_samples();

        /**
         * @brief adjust the strategy with the updates variables
         */
        void resize(size_t const number_of_samples, size_t const gpu_memory);

        /**
         * @brief method which call the gpu kernels used for ddtr
         */
        void exec_kernel(std::size_t dm_range_index
                       , data::FrequencyTime<Cuda, NumericalRep>& d_input
                       , float* d_output
                       , int t_processed
                       , int bin
                       ) const;

    private:
        /**
         * @brief Computes the dedispersion strategy
         *
         */
        void make_strategy(size_t gpu_memory);

    private:
        std::size_t _gpu_memory;
        unsigned int _range;
        std::vector<Dm> _dm_low;
        std::vector<Dm> _dm_high;
        std::vector<Dm> _dm_step;
        std::vector<int> _in_bin;
        unsigned int _maxshift;
        std::vector<float> _dmshifts;
        std::vector<int> _ndms;
        unsigned int _total_ndms;
        Dm _max_dm;
        TimeType _tsamp;
        unsigned int _nsamp;
        unsigned int _nchans;
        FrequencyType _fch1;
        FrequencyType _foff;
        std::size_t _dedispersed_time_samples;
        DmConstantType _dm_constant;
};



} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/ddtr/gpu_bruteforce/detail/DedispersionStrategy.cpp"

#endif // SKA_CHEETAH_MODULES_DDTR_GPU_BRUTEFORCE_DEDISPERSIONSTRATEGY_H
