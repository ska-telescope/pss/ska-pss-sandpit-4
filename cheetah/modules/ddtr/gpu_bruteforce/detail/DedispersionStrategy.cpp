/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/ddtr/gpu_bruteforce/detail/Kernel.h"
#include "cheetah/modules/ddtr/gpu_bruteforce/detail/GpuConstraints.h"
#include "cheetah/cuda_utils/cuda_errorhandling.h"
#include "cheetah/data/TimeFrequency.h"
#include "cheetah/data/Units.h"
#include "panda/Resource.h"
#include "panda/Log.h"
#include <memory>
#include <algorithm>
#include <limits>

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace gpu_bruteforce{

template<class Arch, typename NumericalRep>
DedispersionStrategy<Arch, NumericalRep>::DedispersionStrategy(const data::TimeFrequency<Cpu,NumericalRep>& chunk, const ddtr::DedispersionTrialPlan& plan, std::size_t gpu_memory)
{
    _nsamp = plan.dedispersion_samples();
    _tsamp = chunk.sample_interval();
    _nchans = chunk.number_of_channels();
    auto t = chunk.low_high_frequencies();
    _fch1 = t.second;
    _foff = (t.second-t.first)/((double)(_nchans-1));
    _dm_constant = plan.dm_constant();
    int bin = 0;
    for(auto it = plan.begin_range(); it!=plan.end_range(); ++it)
    {
        _dm_low.push_back(it->dm_start());
        _dm_high.push_back(it->dm_end());
        _dm_step.push_back(it->dm_step());
        _in_bin.push_back(1<<bin++);
    }
    _range = _dm_low.size();
    make_strategy(gpu_memory);
}

template<class Arch, typename NumericalRep>
DedispersionStrategy<Arch, NumericalRep>::~DedispersionStrategy()
{
}

template<class Arch, typename NumericalRep>
unsigned int DedispersionStrategy<Arch, NumericalRep>::range() const
{
    return _range;
}

template<class Arch, typename NumericalRep>
std::vector<int> const& DedispersionStrategy<Arch, NumericalRep>::in_bin() const
{
    return _in_bin;
}

template<class Arch, typename NumericalRep>
unsigned int DedispersionStrategy<Arch, NumericalRep>::maxshift() const
{
    return _maxshift;
}

template<class Arch, typename NumericalRep>
std::vector<typename DedispersionStrategy<Arch, NumericalRep>::Dm> const& DedispersionStrategy<Arch, NumericalRep>::dm_low() const
{
    return _dm_low;
}

template<class Arch, typename NumericalRep>
std::vector<typename DedispersionStrategy<Arch, NumericalRep>::Dm> const& DedispersionStrategy<Arch, NumericalRep>::dm_high() const
{
    return _dm_high;
}

template<class Arch, typename NumericalRep>
std::vector<typename DedispersionStrategy<Arch, NumericalRep>::Dm> const& DedispersionStrategy<Arch, NumericalRep>::dm_step() const
{
    return _dm_step;
}

template<class Arch, typename NumericalRep>
std::vector<float> const& DedispersionStrategy<Arch, NumericalRep>::dmshifts() const
{
    return _dmshifts;
}

template<class Arch, typename NumericalRep>
std::vector<int> const& DedispersionStrategy<Arch, NumericalRep>::ndms() const
{
    return _ndms;
}

template<class Arch, typename NumericalRep>
unsigned int DedispersionStrategy<Arch, NumericalRep>::total_ndms() const
{
    return _total_ndms;
}

template<class Arch, typename NumericalRep>
typename DedispersionStrategy<Arch, NumericalRep>::Dm DedispersionStrategy<Arch, NumericalRep>::max_dm() const
{
    return _max_dm;
}

template<class Arch, typename NumericalRep>
typename DedispersionStrategy<Arch, NumericalRep>::TimeType DedispersionStrategy<Arch, NumericalRep>::tsamp() const
{
    return _tsamp;
}

template<class Arch, typename NumericalRep>
unsigned int DedispersionStrategy<Arch, NumericalRep>::nsamp() const
{
    return _nsamp;
}

template<class Arch, typename NumericalRep>
unsigned int DedispersionStrategy<Arch, NumericalRep>::nchans() const
{
    return _nchans;
}

template<class Arch, typename NumericalRep>
void DedispersionStrategy<Arch, NumericalRep>::resize(size_t const number_of_samples, size_t const gpu_memory)
{
    if(number_of_samples == (std::size_t)_nsamp && gpu_memory == _gpu_memory)
    {
        return;
    }
    _nsamp = number_of_samples;

    make_strategy(gpu_memory);
}

template<class Arch, typename NumericalRep>
void DedispersionStrategy<Arch, NumericalRep>::make_strategy(size_t const gpu_memory)
{
    _gpu_memory = gpu_memory;
    _maxshift = 0;
    _ndms.resize(_range,0);
    _dmshifts.resize(_nchans,0);

    for (unsigned int c = 0; c < _nchans; ++c)
    {
        _dmshifts[c] = std::abs((float)(_dm_constant.value() * ((1.0 / pow(_fch1.value()-_foff.value()*c, 2.0f )) - (1.0 / pow(_fch1.value(), 2.0f))))/_tsamp.value());
    }

    _maxshift = _dmshifts[_nchans-1]*_dm_high[_range-1].value();
    _maxshift = std::ceil(((double)_maxshift/(double)GpuConstraints::threads_per_block))
                                             *GpuConstraints::threads_per_block;
    _nsamp = std::ceil(((double)_nsamp/(double)GpuConstraints::threads_per_block))
                                             *GpuConstraints::threads_per_block;
    _dedispersed_time_samples = _nsamp - _maxshift;

    _total_ndms = 0;
    for(unsigned int i=0; i<_range; i++)
    {
        _ndms[i] = std::floor((_dm_high[i]-_dm_low[i])/_dm_step[i]);
        _total_ndms += _ndms[i];
    }

    std::size_t total_size = _total_ndms*_dedispersed_time_samples*sizeof(float) + sizeof(NumericalRep)*_nsamp;

    if(total_size > 0.75*_gpu_memory)
    {
        PANDA_LOG_ERROR << "Gpu memory is insufficient please check the search parameters. required: "
                        << total_size << " available: " << _gpu_memory;
        throw panda::Error("Gpu is not compatible for the selected dedispersion plan");
    }
}

template<class Arch, typename NumericalRep>
void DedispersionStrategy<Arch, NumericalRep>::exec_kernel(std::size_t dm_range_index
                                                          , data::FrequencyTime<Cuda, NumericalRep>& d_input
                                                          , float* d_output
                                                          , int t_processed
                                                          , int bin
                                                          ) const
{

    if(bin!=1)
    {
        Kernel<NumericalRep>::bin_gpu(d_input, t_processed, bin, GpuConstraints::threads_per_block);
    }
    Kernel<NumericalRep>::exec(dm_range_index, *this, d_input, d_output, t_processed, bin, GpuConstraints::threads_per_block);
}

template<class Arch, typename NumericalRep>
data::DimensionSize<data::Time> DedispersionStrategy<Arch, NumericalRep>::dedispersed_samples()
{
    return data::DimensionSize<data::Time>(_dedispersed_time_samples);
}

} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
