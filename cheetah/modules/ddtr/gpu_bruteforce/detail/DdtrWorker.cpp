/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/ddtr/gpu_bruteforce/detail/DdtrWorker.h"
#include "cheetah/modules/ddtr/gpu_bruteforce/DdtrProcessor.h"
#include "cheetah/modules/ddtr/gpu_bruteforce/detail/DedispersionStrategy.cuh"
#include "cheetah/data/FrequencyTime.h"
#include "cheetah/data/TimeSeries.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace gpu_bruteforce {


template<typename DdtrTraits>
DdtrWorker<DdtrTraits>::DdtrWorker(Config const& config)
    : _config(config)
{
}

template<typename DdtrTraits>
template<typename BufferType, typename CallBackT>
std::shared_ptr<typename DdtrWorker<DdtrTraits>::DmTrialsType> DdtrWorker<DdtrTraits>::operator()(
                                                                      panda::PoolResource<panda::nvidia::Cuda>& gpu
                                                                    , BufferType const& agg_buf
                                                                    , std::shared_ptr<DedispersionPlan<DdtrTraits>> plan
                                                                    , CallBackT const& call_back)
{
    PANDA_LOG_DEBUG << "gpu_bruteforce:: invoked (on device " << gpu.device_id() << ")";

    if (agg_buf.data_size() < (std::size_t) plan->dedispersion_strategy()->maxshift())
    {
        panda::Error e("DdtrCuda: data buffer size < maxshift (");
        e << agg_buf.data_size() << "<" << plan->dedispersion_strategy()->maxshift() << ")";
        throw e;
    }
    auto const& data = agg_buf.buffer();

    // cornerturn and transfer data to device
    data::FrequencyTime<Cuda, NumericalRep> device_ft_data(data);

    auto dm_trials_ptr = DmTrialsType::make_shared(plan->dm_trial_metadata(), device_ft_data.start_time());
    std::size_t current_dm_range = 0;
    DdtrProcessor<DdtrTraits> ddtr(*plan->dedispersion_strategy()
                                , device_ft_data, dm_trials_ptr
                                , _config.copy_dmtrials_to_host()
                                , current_dm_range
                                );

    while(!ddtr.finished())
    {
        cudaDeviceSynchronize();
        ++ddtr;
        call_back(const_cast<DdtrProcessor<DdtrTraits> const&>(ddtr));
        ++current_dm_range;
    }

    return dm_trials_ptr;
}

} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
