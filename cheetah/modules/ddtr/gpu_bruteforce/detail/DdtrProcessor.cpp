/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "cheetah/modules/ddtr/gpu_bruteforce/DdtrProcessor.h"
#include "cheetah/data/FrequencyTime.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace gpu_bruteforce {

template<typename DdtrTraits>
DdtrProcessor<DdtrTraits>::DdtrProcessor(DedispersionStrategyType const& strategy
                                        , data::FrequencyTime<cheetah::Cuda, NumericalRep>& data
                                        , std::shared_ptr<DmTrialsType> dm_trials_ptr
                                        , bool copy_flag
                                        , size_t& current_dm_range
                            )
    : _gpu_strategy(strategy)
    , _device_ft_data(data)
    , _strategy(strategy)
    , _dm_trials_ptr(dm_trials_ptr)
    , _copy_dm_trials(copy_flag)
    , _current_dm_range(current_dm_range)
{
}

template<typename DdtrTraits>
bool DdtrProcessor<DdtrTraits>::finished() const
{
    return _current_dm_range >= _strategy.range();
}

template<typename DdtrTraits>
typename DdtrProcessor<DdtrTraits>::GpuStrategyType& DdtrProcessor<DdtrTraits>::gpu_strategy()
{
    return _gpu_strategy;
}

template<typename DdtrTraits>
typename DdtrProcessor<DdtrTraits>::DedispersionStrategyType const& DdtrProcessor<DdtrTraits>::dedispersion_strategy() const
{
    return _strategy;
}

template<typename DdtrTraits>
std::size_t const& DdtrProcessor<DdtrTraits>::current_dm_range() const
{
    return _current_dm_range;
}

template<typename DdtrTraits>
DdtrProcessor<DdtrTraits>& DdtrProcessor<DdtrTraits>::operator++()
{
    _strategy.exec_kernel(_current_dm_range
                        , _device_ft_data
                        , thrust::raw_pointer_cast(&*_gpu_strategy.ddtr_output().begin())
                        , (int)(_strategy.nsamp()-_strategy.maxshift())
                        , _strategy.in_bin()[_current_dm_range]
                        );
    DmTrialsType& dmtrials = *(_dm_trials_ptr);

    if(_copy_dm_trials==true)
    {
        for(std::size_t dmidx=0; dmidx<dmtrials.size(); ++dmidx)
        {
            panda::copy(_gpu_strategy.ddtr_output().begin() + dmtrials.metadata().number_of_samples()*dmidx
                      , _gpu_strategy.ddtr_output().begin() + dmtrials.metadata().number_of_samples()*(dmidx+1)-1
                      , dmtrials[dmidx].begin());


        }
    }

    return *this;
}

} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska
