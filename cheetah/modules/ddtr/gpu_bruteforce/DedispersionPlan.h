/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016-2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_DDTR_GPU_BRUTEFORCE_DEDISPERSIONPLAN_H
#define SKA_CHEETAH_MODULES_DDTR_GPU_BRUTEFORCE_DEDISPERSIONPLAN_H

#include "cheetah/modules/ddtr/gpu_bruteforce/Config.h"
#include "cheetah/modules/ddtr/gpu_bruteforce/DedispersionStrategy.h"
#include "cheetah/modules/ddtr/Config.h"
#include "cheetah/data/TimeFrequency.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace ddtr {
namespace gpu_bruteforce {

/**
 * @brief Dedipsersion plan for the gpu_bruteforce module
 */

template <typename DdtrTraits>
class DedispersionPlan
{
    private:
        typedef typename DdtrTraits::value_type NumericalT;
        typedef DedispersionStrategy<Cpu, NumericalT> DedispersionStrategyType;

    public:
        typedef typename DdtrTraits::TimeFrequencyType TimeFrequencyType;
        typedef ddtr::Config::Dm Dm;
        typedef typename TimeFrequencyType::FrequencyType FrequencyType;
        typedef typename TimeFrequencyType::TimeType TimeType;
        typedef std::vector<FrequencyType> FrequencyListType;
        typedef ddtr::DedispersionTrialPlan ConfigType;

    public:
        DedispersionPlan(ConfigType const& config, std::size_t memory=0);

        /**
        * @brief takes in TF chunk and gernerates a strategy object returning the
        *        number of spectra.
        */
        data::DimensionSize<data::Time> reset(TimeFrequencyType const&);

        /**
        * @brief sets the number of spectra private member
        */
        void reset(data::DimensionSize<data::Time> const& spectra);

        /**
        * @brief returns number of spectra
        */
        data::DimensionSize<data::Time> number_of_spectra() const;

        /**
        * @brief essentially returns maxshift
        */
        data::DimensionSize<data::Time> buffer_overlap() const;

        /**
        * @brief generates dmtrials metadata
        */
        std::shared_ptr<data::DmTrialsMetadata> generate_dmtrials_metadata(TimeType sample_interval, std::size_t nspectra) const;

        /**
        * @brief returns dmtrials metadata
        */
        std::shared_ptr<data::DmTrialsMetadata> dm_trial_metadata() const;

        /**
        * @brief returns pointer to the DedispersionStrategy
        */
        std::shared_ptr<DedispersionStrategyType> const& dedispersion_strategy() const;

        /**
        * @brief returns algo_config
        */
        ConfigType const& algo_config() const;

    private:
        ConfigType const& _algo_config;
        std::shared_ptr<DedispersionStrategyType> _strategy;
        std::shared_ptr<data::DmTrialsMetadata> _dm_trial_metadata;
        std::size_t _memory;
        std::size_t _max_delay;
        std::size_t _dedispersion_samples;
        std::vector<double> _dm_factors;
        std::size_t _number_of_spectra;
};

} // namespace gpu_bruteforce
} // namespace ddtr
} // namespace modules
} // namespace cheetah
} // namespace ska

#include "cheetah/modules/ddtr/gpu_bruteforce/detail/DedispersionPlan.cpp"
#endif // SKA_CHEETAH_MODULES_DDTR_GPU_BRUTEFORCE_DEDISPERSIONPLAN_H
