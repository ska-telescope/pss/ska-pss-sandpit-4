/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_MODULES_FFT_FFTTRAITS_H
#define SKA_CHEETAH_MODULES_FFT_FFTTRAITS_H

#include "cheetah/modules/fft/detail/AlgoWrapper.h"
#include "cheetah/modules/fft/detail/HasFftProcessMethod.h"
#include "cheetah/modules/fft/detail/MethodConfig.h"
#include "cheetah/data/TimeSeries.h"
#include "cheetah/data/FrequencySeries.h"
#include "panda/AlgorithmInfo.h"
#include "panda/ConfigurableTask.h"
#include "panda/TupleUtilities.h"
#include "panda/TypeTag.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace fft {

class CommonPlan;
class InputDataReshaper;

namespace
{
    // helpers to generate the submit method signatures covering all the required data types
    template<template <typename> class InputDataT, template<typename> class OutputDataT, class FftAlgoTuple>
    class MethodGenerator {
            template<typename Algo>
            using Matcher = fft::HasFftProcessMethod<Algo, InputDataT<typename Algo::Architecture>, OutputDataT<typename Algo::Architecture>>;

            template<class... Algos>
            using AlgoExtractor = panda::extract_matching<Matcher, Algos...>;

        public:
            typedef typename panda::TypeTransfer<FftAlgoTuple, AlgoExtractor>::type::type SupportingAlgos;

        private:
            typedef typename cheetah::utils::System::SupportedArchitectures Architectures;

            // each of these template alias' specify methods that will generated for each Architecture.
            template<class Arch> using Method = panda::SubmitMethod<CommonPlan, std::shared_ptr<InputDataT<Arch>> const&, panda::TypeTag<OutputDataT<Arch>>>;
            template<class Arch> using SyncSignature = panda::Signature<CommonPlan, InputDataT<Arch> const&, OutputDataT<Arch>&>;
            template<class Arch> using ShaperMethodSig = panda::Signature<CommonPlan&, InputDataT<Arch> const&, panda::TypeTag<OutputDataT<Arch>>>;
            template<class... Archs> using ShaperMethod = panda::Method<InputDataReshaper&, ShaperMethodSig<Archs>...>;


            typedef typename panda::TupleExpander<Method, Architectures>::type SubmitMethods;
            typedef typename panda::TupleExpander<SyncSignature, Architectures>::type SyncMethods;
            template<typename... Algos> using MethodConfigTmpl = MethodConfig<typename Algos::Config...>;

        public:
            typedef typename panda::TypeTransfer<Architectures, ShaperMethod>::type ShaperType;
            typedef typename panda::join_tuples<std::tuple<ShaperType>, SyncMethods, SubmitMethods>::type type;
            typedef typename panda::TypeTransfer<SupportingAlgos, MethodConfigTmpl>::type ConfigType;
    };


    template<template<typename> class InputDataTmpl
           , template<typename> class OutputDataTmpl
           , class FftAlgoTupleT>
    class SingleMethodTraits
    {
            typedef MethodGenerator<InputDataTmpl, OutputDataTmpl, FftAlgoTupleT> Generator;

        public:
            typedef typename Generator::type Signatures;
            typedef typename panda::TupleExpander<AlgoWrapper, typename Generator::SupportingAlgos>::type Algos;
            typedef typename Generator::ConfigType ConfigType;
    };

} // namespace (end of helpers)

/**
 * @brief Traits for the complex FrequencySeries -> TimeSeries FftModule
 */
template<typename CommonTraits, class FftAlgoTupleT>
class FreqC2CTraits : public CommonTraits
{
        typedef typename CommonTraits::value_type ValueT;
        template<class ArchT>
        using ComplexT = typename data::ComplexTypeTraits<ArchT, ValueT>::type;

    public:
        template<typename ArchT>
        using OutputTmpl = data::TimeSeries<ArchT, ComplexT<ArchT>>;

    private:
        template<typename ArchT>
        using InputSeriesTmpl = data::FrequencySeries<ArchT, ComplexT<ArchT>>;

        typedef SingleMethodTraits<InputSeriesTmpl, OutputTmpl, FftAlgoTupleT> MethodTraits;

    public:
        typedef typename MethodTraits::Signatures Signatures;
        typedef typename MethodTraits::Algos Algos;
        typedef typename MethodTraits::ConfigType ConfigType;
};

/**
 * @brief Traits for the complex FrequencySeries -> real TimeSeries FftModule
 */
template<typename CommonTraits, class FftAlgoTupleT>
class FreqC2RTraits : public CommonTraits
{
        typedef typename CommonTraits::value_type ValueT;
        template<class ArchT>
        using ComplexT = typename data::ComplexTypeTraits<ArchT, ValueT>::type;

    public:
        template<typename ArchT>
        using OutputTmpl = data::TimeSeries<ArchT, ValueT>;

    private:
        template<typename ArchT>
        using InputSeriesTmpl = data::FrequencySeries<ArchT, ComplexT<ArchT>>;

        typedef SingleMethodTraits<InputSeriesTmpl, OutputTmpl, FftAlgoTupleT> MethodTraits;

    public:
        typedef typename MethodTraits::Signatures Signatures;
        typedef typename MethodTraits::Algos Algos;
        typedef typename MethodTraits::ConfigType ConfigType;
};

/**
 * @brief Traits for the complex TimeSeries -> FrequencySeries FftModule
 */
template<typename CommonTraits, class FftAlgoTupleT>
class TimeC2CTraits : public CommonTraits
{
        typedef typename CommonTraits::value_type ValueT;
        template<class ArchT>
        using ComplexT = typename data::ComplexTypeTraits<ArchT, ValueT>::type;

    public:
        template<typename ArchT>
        using OutputTmpl = data::FrequencySeries<ArchT, ComplexT<ArchT>>;

    private:
        template<typename ArchT>
        using InputSeriesTmpl = data::TimeSeries<ArchT, ComplexT<ArchT>>;

        typedef SingleMethodTraits<InputSeriesTmpl, OutputTmpl, FftAlgoTupleT> MethodTraits;

    public:
        typedef typename MethodTraits::Signatures Signatures;
        typedef typename MethodTraits::Algos Algos;
        typedef typename MethodTraits::ConfigType ConfigType;
};

/**
 * @brief Traits for the real TimeSeries -> complex FrequencySeries FftModule
 */
template<typename CommonTraits, class FftAlgoTupleT>
class TimeR2CTraits : public CommonTraits
{
        typedef typename CommonTraits::value_type ValueT;

        template<typename ArchT>
        using TimeSeriesType = data::TimeSeries<ArchT, ValueT>;

    public:
        template<typename ArchT>
        using OutputTmpl = data::FrequencySeries<ArchT, typename data::ComplexTypeTraits<ArchT, ValueT>::type>;

    private:
        typedef SingleMethodTraits<TimeSeriesType, OutputTmpl, FftAlgoTupleT> MethodTraits;

    public:
        typedef typename MethodTraits::Signatures Signatures;
        typedef typename MethodTraits::Algos Algos;
        typedef typename MethodTraits::ConfigType ConfigType;
};


/**
 * @brief Fft Traits specified at the top level Fft class (i.e. API user)
 */
template<typename NumericalT, typename HandlerT>
struct FftTraits {
    typedef HandlerT Handler;
    typedef NumericalT value_type;
};


} // namespace fft
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_FFT_FFTTRAITS_H
