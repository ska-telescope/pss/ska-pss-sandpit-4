/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/brdz/test_utils/BrdzTester.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/utils/Architectures.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace brdz {
namespace test {

template<typename ArchitectureTag, typename ArchitectureCapability>
BrdzTesterTraits<ArchitectureTag,ArchitectureCapability>::BrdzTesterTraits()
    : _api(_config)
{
}

template<typename ArchitectureTag, typename ArchitectureCapability>
brdz::Brdz& BrdzTesterTraits<ArchitectureTag,ArchitectureCapability>::api()
{
    return _api;
}

template<typename ArchitectureTag, typename ArchitectureCapability>
brdz::Config& BrdzTesterTraits<ArchitectureTag,ArchitectureCapability>::config()
{
    return _config;
}

template <typename DeviceType, typename Arch, typename T >
struct ExcisionTest
{
    inline static void test(DeviceType& device, brdz::Brdz& api, brdz::Config& config)
    {
        typedef typename data::ComplexTypeTraits<Arch,T>::type ComplexType;
        typedef data::FrequencySeries<Arch,ComplexType> DataType;
        auto fill_value = ComplexType(1.0, 1.0);
        auto replace_value = ComplexType(0.0, 0.0);
        DataType input(0.001 * data::hz);
        input.resize(1<<23,fill_value);
        std::vector<data::Birdie> birdies;
        data::FourierFrequencyType width(5 * 0.001 * data::hz);
        for (int ii=100;ii<(1<<23);ii+=10000)
        {
            auto freq = input.bin_to_frequency(ii);
            birdies.push_back(data::Birdie(freq,width));
        }
        config.birdies(birdies.begin(),birdies.end());
        api.process<Arch,T,typename DataType::Allocator>(device,input);
        for (auto& birdie: birdies)
        {
            std::size_t lower = input.frequency_to_bin(birdie.frequency()-birdie.width()/2.0);
            std::size_t upper = input.frequency_to_bin(birdie.frequency()+birdie.width()/2.0);
            lower = std::max(std::size_t(0),lower);
            upper = std::min(upper,input.size());
            for (std::size_t bin = lower; bin < upper; ++bin)
            {
                ComplexType x = input[bin];
                ASSERT_EQ(x,replace_value);
            }
        }
    }
};

template <typename TestTraits>
BrdzTester<TestTraits>::BrdzTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
BrdzTester<TestTraits>::~BrdzTester()
{
}

template<typename TestTraits>
void BrdzTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void BrdzTester<TestTraits>::TearDown()
{
}

ALGORITHM_TYPED_TEST_P(BrdzTester, test_excision)
{
    TypeParam traits;
    ExcisionTest<typename TypeParam::DeviceType, typename TypeParam::Arch, float>::test(device,traits.api(),traits.config());
    ExcisionTest<typename TypeParam::DeviceType, typename TypeParam::Arch, double>::test(device,traits.api(),traits.config());
}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_CASE_P(BrdzTester, test_excision);

} // namespace test
} // namespace brdz
} // namespace modules
} // namespace cheetah
} // namespace ska
