/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/brdz/Config.h"
#include <type_traits>

namespace ska {
namespace cheetah {
namespace modules {
namespace brdz {

template <typename Container>
void Config::birdies(Container&& birds)
{
    static_assert(std::is_same<typename Container::value_type, data::Birdie>::value,
        "Container must have have value type of data::Birdie");
    _birdies = birds;
}

template <typename Iterator>
void Config::birdies(Iterator beg, Iterator end)
{
    static_assert(std::is_same<typename Iterator::value_type, data::Birdie>::value,
        "Container must have have value type of data::Birdie");
    _birdies.assign(beg,end);
}

template <typename Container>
void Config::birdies(Container const& birds)
{
    static_assert(std::is_same<typename Container::value_type, data::Birdie>::value,
        "Container must have have value type of data::Birdie");
    _birdies = birds;
}


} // namespace brdz
} // namespace modules
} // namespace cheetah
} // namespace ska
