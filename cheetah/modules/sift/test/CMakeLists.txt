include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_sift_src src/gtest_sift.cpp)

set(gtest_sift_src_simpleSift)

add_executable(gtest_sift ${gtest_sift_src} ${gtest_sift_src_simpleSift})
target_link_libraries(gtest_sift ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
add_test(gtest_sift gtest_sift --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
