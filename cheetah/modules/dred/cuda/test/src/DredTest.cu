#include "cheetah/modules/dred/cuda/Dred.cuh"
#include "cheetah/modules/dred/test_utils/DredTester.h"

namespace ska {
namespace cheetah {
namespace modules {
namespace dred {
namespace cuda {
namespace test {

template <typename T>
struct CudaTraits
    : public dred::test::DredTesterTraits<typename dred::cuda::Dred<T>::Architecture, typename dred::cuda::Dred<T>::ArchitectureCapability, T>
{
    typedef dred::test::DredTesterTraits<typename dred::cuda::Dred<T>::Architecture, typename dred::cuda::Dred<T>::ArchitectureCapability, T> BaseT;
    typedef typename Dred<T>::Architecture Arch;
    typedef typename BaseT::DeviceType DeviceType;
};

} // namespace test
} // namespace cuda
} // namespace dred
} // namespace modules
} // namespace cheetah
} // namespace ska

namespace ska {
namespace cheetah {
namespace modules {
namespace dred {
namespace test {

typedef ::testing::Types<cuda::test::CudaTraits<float>,cuda::test::CudaTraits<double>> CudaTraitsTypes;
INSTANTIATE_TYPED_TEST_CASE_P(Cuda, DredTester, CudaTraitsTypes);

} // namespace test
} // namespace dred
} // namespace modules
} // namespace cheetah
} // namespace ska