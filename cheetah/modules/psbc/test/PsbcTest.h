#ifndef SKA_CHEETAH_MODULES_PSBC_PSBCTEST_H
#define SKA_CHEETAH_MODULES_PSBC_PSBCTEST_H

#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace modules {
namespace psbc {
namespace test {

class PsbcTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

    public:
        PsbcTest();
        ~PsbcTest();

    private:
};

} // namespace test
} // namespace psbc
} // namespace modules
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_MODULES_PSBC_PSBCTEST_H