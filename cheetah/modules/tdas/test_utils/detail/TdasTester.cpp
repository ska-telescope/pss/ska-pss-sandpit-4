/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/tdas/test_utils/TdasTester.h"
#include "cheetah/data/DmTrialsMetadata.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/Ccl.h"
#include "cheetah/data/Units.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/ComplexTypeTraits.h"
#include "cheetah/data/test_utils/DmTimeTest.h"

#include <iostream>
#include <memory>

namespace ska {
namespace cheetah {
namespace modules {
namespace tdas {
namespace test {

template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
TdasTesterTraits<ArchitectureTag,ArchitectureCapability, T>::TdasTesterTraits()
    : _api(_config)
{
}

template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
tdas::Tdas<T>& TdasTesterTraits<ArchitectureTag,ArchitectureCapability, T>::api()
{
    return _api;
}

template<typename ArchitectureTag, typename ArchitectureCapability, typename T>
tdas::Config& TdasTesterTraits<ArchitectureTag,ArchitectureCapability, T>::config()
{
    return _config;
}

template <typename DeviceType, typename Arch, typename T>
struct ExecTest
{
    inline static void test(DeviceType& device, tdas::Tdas<T>& api)
    {
        typedef typename utils::ModifiedJulianClock::time_point TimePoint;
        typedef typename data::SecondsType<double> Seconds;
        typedef typename tdas::DmTrialsType::DmType Dm;
        std::size_t number_of_blocks = 10L;
        auto buffer = tdas::DmTimeType::make_shared();
        typename utils::ModifiedJulianClock::time_point epoch(utils::julian_day(40587.0));
        for (std::size_t block=0; block<number_of_blocks; ++block)
        {
            auto metadata = data::DmTrialsMetadata::make_shared(Seconds(0.0001*data::seconds),1<<14);
            metadata->emplace_back(Dm(0.0*data::parsecs_per_cube_cm),1);
            metadata->emplace_back(Dm(10.0*data::parsecs_per_cube_cm),2);
            metadata->emplace_back(Dm(20.0*data::parsecs_per_cube_cm),4);
            auto trials = tdas::DmTrialsType::make_shared(metadata,epoch);
            buffer->add(trials);
        }
        auto it = buffer->begin(2);
        auto slice = *it;
        std::shared_ptr<data::Ccl> candidate_list = api.process(device,*slice);
    }
};

template <typename TestTraits>
TdasTester<TestTraits>::TdasTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
TdasTester<TestTraits>::~TdasTester()
{
}

template<typename TestTraits>
void TdasTester<TestTraits>::SetUp()
{
}

template<typename TestTraits>
void TdasTester<TestTraits>::TearDown()
{
}

ALGORITHM_TYPED_TEST_P(TdasTester, test_exec)
{
    TypeParam traits;
    auto& config = traits.config();
    auto& api = traits.api();
    ExecTest<typename TypeParam::DeviceType,typename TypeParam::Arch, typename TypeParam::ValueType>::test(device,api);
}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_CASE_P(TdasTester, test_exec);

} // namespace test
} // namespace tdas
} // namespace modules
} // namespace cheetah
} // namespace ska
