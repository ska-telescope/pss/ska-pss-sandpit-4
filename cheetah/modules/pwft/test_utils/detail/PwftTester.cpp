/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "cheetah/modules/pwft/test_utils/PwftTester.h"
#include "cheetah/data/FrequencySeries.h"
#include "cheetah/data/PowerSeries.h"
#include "cheetah/utils/Architectures.h"
#include "cheetah/data/ComplexTypeTraits.h"

#include <iostream>
#include <typeinfo>

namespace ska {
namespace cheetah {
namespace modules {
namespace pwft {
namespace test {

template<typename ArchitectureTag, typename ArchitectureCapability>
PwftTesterTraits<ArchitectureTag,ArchitectureCapability>::PwftTesterTraits()
    : _api(_config)
{
}

template<typename ArchitectureTag, typename ArchitectureCapability>
pwft::Pwft& PwftTesterTraits<ArchitectureTag,ArchitectureCapability>::api()
{
    return _api;
}


template <typename DeviceType, typename InputDataType, typename OutputDataType>
struct FrequencyStepPropagationTest
{
    inline static void test_direct(DeviceType& device, pwft::Pwft& api)
    {
        InputDataType input(0.001 * data::hz);
        input.resize(20);
        OutputDataType output;
        api.process_direct(device,input,output);
        ASSERT_EQ(output.frequency_step(),input.frequency_step());
    }

    inline static void test_nn(DeviceType& device, pwft::Pwft& api)
    {
        InputDataType input(0.002 * data::hz);
        input.resize(20);
        OutputDataType output;
        api.process_nn(device,input,output);
        ASSERT_EQ(output.frequency_step(),input.frequency_step());
    }
};

template <typename TestTraits>
PwftTester<TestTraits>::PwftTester()
    : cheetah::utils::test::AlgorithmTester<TestTraits>()
{
}

template <typename TestTraits>
PwftTester<TestTraits>::~PwftTester()
{
}

template<typename TestTraits>
void PwftTester<TestTraits>::SetUp()
{

}

template<typename TestTraits>
void PwftTester<TestTraits>::TearDown()
{
}

ALGORITHM_TYPED_TEST_P(PwftTester, test_frequency_step_propagation)
{
    TypeParam traits;
    typedef typename data::ComplexTypeTraits<typename TypeParam::Arch,float>::type ComplexType;
    typedef data::FrequencySeries<typename TypeParam::Arch,ComplexType> InputType;
    typedef data::PowerSeries<typename TypeParam::Arch,float> OutputType;
    FrequencyStepPropagationTest<typename TypeParam::DeviceType, InputType, OutputType>::test_direct(device,traits.api());
    FrequencyStepPropagationTest<typename TypeParam::DeviceType, InputType, OutputType>::test_nn(device,traits.api());
}

// each test defined by ALGORITHM_TYPED_TEST_P must be added to the
// test register (each one as an element of the comma seperated list)
REGISTER_TYPED_TEST_CASE_P(PwftTester, test_frequency_step_propagation);

} // namespace test
} // namespace pwft
} // namespace modules
} // namespace cheetah
} // namespace ska
