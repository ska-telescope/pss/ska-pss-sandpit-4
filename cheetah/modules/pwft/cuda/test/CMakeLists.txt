include_directories(${GTEST_INCLUDE_DIR})
link_directories(${GTEST_LIBRARY_DIR})

set(gtest_pwft_cuda_src
    src/PwftTest.cu
    src/gtest_pwft_cuda.cu
)

if(ENABLE_CUDA)
    cuda_add_executable(gtest_pwft_cuda ${gtest_pwft_cuda_src})
    target_link_libraries(gtest_pwft_cuda ${CHEETAH_TEST_UTILS} ${CHEETAH_LIBRARIES} ${GTEST_LIBRARIES})
    add_test(gtest_pwft_cuda gtest_pwft_cuda --test_data "${CMAKE_CURRENT_LIST_DIR}/data")
endif()
