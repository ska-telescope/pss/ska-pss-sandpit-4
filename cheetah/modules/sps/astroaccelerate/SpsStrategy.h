/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef SKA_CHEETAH_MODULES_SPS_ASTROACCELERATE_SPSSTRATEGY_H
#define SKA_CHEETAH_MODULES_SPS_ASTROACCELERATE_SPSSTRATEGY_H

#include "cheetah/modules/ddtr/astroaccelerate/DedispersionStrategy.h"
#include "cheetah/data/Units.h"
#include "cheetah/data/TimeFrequency.h"
#include <memory>
#include <algorithm>
#include <limits>

namespace ska {
namespace cheetah {
namespace modules {
namespace sps {
namespace astroaccelerate {

struct IterationDetails {

    IterationDetails();

    int decimated_timesamples() const;

    void decimated_timesamples(int value);

    int dtm() const;

    void dtm(int value);

    int iteration() const;

    void iteration(int value);

    int number_blocks() const;

    void number_blocks(int value);

    int number_boxcars() const;

    void number_boxcars(int value);

    int output_shift() const;

    void output_shift(int value);

    int shift() const;

    void shift(int value);

    int start_taps() const;

    void start_taps(int value);

    int total_unprocessed() const;

    void total_unprocessed(int value);

    int unprocessed_samples() const;

    void unprocessed_samples(int value);

    private:
        int _decimated_timesamples;
        int _dtm;
        int _iteration;
        int _number_blocks;
        int _number_boxcars;
        int _output_shift;
        int _shift;
        int _start_taps;
        int _total_unprocessed;
        int _unprocessed_samples;
};

template <typename NumericalT>
class SpsOptimizationParameters : public modules::ddtr::astroaccelerate::DedispersionOptimizationParameters<NumericalT>
{
    public:
        static constexpr unsigned PD_NTHREADS = 512;
        static constexpr unsigned THR_WARPS_PER_BLOCK = 4;
        static constexpr unsigned PD_MAXTAPS = 32;
        static constexpr unsigned WARP = 32;
        static constexpr unsigned THR_ELEM_PER_THREAD = 4;
};

template<class Arch, typename NumericalT, typename OptimizationParameterT=SpsOptimizationParameters<NumericalT>>
class SpsStrategy
{
    public:
        typedef NumericalT NumericalRep;
        typedef uint16_t GpuNumericalRep;
        typedef boost::units::quantity<data::MegaHertz, double> FrequencyType;
        typedef data::DedispersionMeasureType<float> Dm;
        typedef boost::units::quantity<data::dm_constant::s_mhz::Unit, double> DmConstantType;
        typedef boost::units::quantity<boost::units::si::time, double> TimeType;
        typedef modules::ddtr::astroaccelerate::DedispersionStrategy<Cpu, NumericalRep> DedispersionStrategyType;

    public:
        SpsStrategy(DedispersionStrategyType const&);
        ~SpsStrategy();

        /**
         * @brief Max number of candidates
         */
        size_t max_candidates() const;

        /**
         * @brief Desired Max boxcar width
         */
        size_t max_boxcar_width_desired() const;

        /**
         * @brief vector containg number of DMs for each range
         */
        std::vector<int> const& ndms() const;

        /**
         * @brief number of Dm ranges
         */
        int nranges() const;

        /**
         * @brief Numbet of time chunks
         */
        int ntchunks() const;

        /**
         * @brief Returns vector of boxcar widths
         */
        std::vector<int> const& boxcar_widths() const;

        /**
         * @brief Algorithm to be used default value is 1
         */
        int candidate_algorithm() const;

        /**
         * @brief Current time samples
         */
        int current_time_samples() const;

        /**
         * @brief Number of iterations
         */
        int iterations() const;

        /**
         * @brief Max width of the boxcar
         */
        size_t max_boxcar_width_performed() const;

        /**
         * @brief Returns boxcar width for a given widths vector and element within the vector.
         *
         * The function checks for the out-of-range access - returns the last element if element beyond the size of the vector is requested.
         *
         * @param element index of the element to return
         * @return int the requested boxcar width
         */
        int get_boxcar_width(int element) const;

        /**
         * @brief generates the processing details
         */
        void generate_processing_details(int current_range, int current_time);

        /**
         * @brief Calculates the number of averaging iterations
         *
         * @param max_boxcar_width boxcar width requested (n.b. may not be fulfilled - the actual width used is returned)
         * @return pair containing the number of SPS averaging interations and maximum boxcar width actually searched with
         */
        std::pair<int, size_t> calculate_max_iterations(int max_boxcar_width);

        /**
         * @brief Generate boxcar widths to be searched for each iteration
         */
        void generate_boxcars_widths();

        /**
         * @brief Thersold used for the detection
         */
        float threshold() const;

        /**
         * @brief returns DedispersionStrategy
         */
        DedispersionStrategyType const& strategy() const;

        /**
         * @brief Cutoff thresold for estimating Stadard deviation (default 10)
         */
        float or_sigma_multiplier() const;

        /**
         * @brief Flag to enable outlier rejection
         */
        int enable_outlier_rejection() const;

        /**
         * @returns Iteration details for a given iteration
         */
        IterationDetails details(int iteration) const;

    private:
        DedispersionStrategyType const& _strategy;
        size_t _max_candidates;
        size_t _max_boxcar_width_desired;
        std::vector<int> _ndms;
        int _nranges;
        int _ntchunks;
        std::vector<int> _boxcar_widths;
        // NOTE: Every iteration will have different strategy
        std::vector<IterationDetails> _details;
        int _candidate_algorithm;
        int _current_time_samples;
        int _iterations;
        size_t _max_boxcar_width_performed;
        std::vector<int> _decimate_bc_limits;
        float _or_sigma_multiplier;
        int _enable_outlier_rejection;
};

} // namespace astroacclerate
} // namespace sps
} // namespace modules
} // namespace cheetah
} // namespace ska
#include "cheetah/modules/sps/astroaccelerate/detail/SpsStrategy.cpp"

#endif // SKA_CHEETAH_MODULES_SPS_ASTROACCELERATE_SPSSTRATEGY_H
