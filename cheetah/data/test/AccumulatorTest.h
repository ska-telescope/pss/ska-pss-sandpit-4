/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2022 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_DATA_TEST_ACCUMULATORTEST_H
#define SKA_CHEETAH_DATA_TEST_ACCUMULATORTEST_H

#include "cheetah/data/Accumulator.h"
#include <gtest/gtest.h>

namespace ska {
namespace cheetah {
namespace data {
namespace test {

/**
 * @brief Class to test the Accumulator class
 * @details
 */

template<typename TypeParam>
class AccumulatorTest : public ::testing::Test
{
    protected:
        void SetUp() override;
        void TearDown() override;

        // An accumulator to be used in most unit tests.
        // Values for TypeParam are listed in AccumulatorTestTypes below.
        TypeParam *_accumulator;

    public:
        AccumulatorTest() = default;
};

using AccumulatorTestTypes = ::testing::Types<data::Accumulator<uint8_t>, data::Accumulator<uint16_t>, data::Accumulator<float>>;
TYPED_TEST_CASE(AccumulatorTest, AccumulatorTestTypes);

} // namespace test
} // namespace data
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_DATA_TEST_ACCUMULATORTEST_H
