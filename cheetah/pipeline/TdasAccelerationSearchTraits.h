/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 The SKA organisation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef SKA_CHEETAH_PIPELINE_TDASACCELERATIONSEARCHTRAITS_H
#define SKA_CHEETAH_PIPELINE_TDASACCELERATIONSEARCHTRAITS_H
#include "cheetah/pipeline/AccelerationSearch.h"
#include "cheetah/pipeline/AccelerationSearchAlgoConfig.h"
#include "cheetah/modules/tdas/Tdas.h"

namespace ska {
namespace cheetah {
namespace pipeline {

/**
 * @brief
 *    AccelrationSearchAlgoTraits to create a Time domain accelerated Search
 *
 */

template<typename NumericalT>
struct TdasAccelerationSearchTraits : public DefaultAccelerationSearchTraits<NumericalT>
{

    template<typename SiftT>
    static inline
    modules::tdas::Tdas<double, SiftT const&>* create_acceleration_search_algo(AccelerationSearchAlgoConfig const& acceleration_search_config, SiftT const& sift)
    {
        return new modules::tdas::Tdas<double, SiftT const&>(acceleration_search_config.tdas_config(), sift);
    }

};

} // namespace pipeline
} // namespace cheetah
} // namespace ska

#endif // SKA_CHEETAH_PIPELINE_TDASACCELERATIONSEARCHTRAITS_H
