if(NOT SKA_CHEETAH_PACKAGING_GUARD_VAR)
    set(SKA_CHEETAH_PACKAGING_GUARD_VAR TRUE)
else()
    return()
endif()

# Packaging
set(CPACK_PACKAGE_VENDOR "The SKAO PSS Team")

# RPM specifics
set(CPACK_RPM_PACKAGE_REQUIRES "libboost >= 1.53")

# Debian specifics
set(CPACK_DEBIAN_PACKAGE_DEPENDS libboost-dev)
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "christopher.williams@physics.ox.ac.uk")

include(CPack)
